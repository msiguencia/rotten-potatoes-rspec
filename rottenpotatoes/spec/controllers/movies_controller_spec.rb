require 'spec_helper'


describe MoviesController do
    describe 'finding movies with same director' do
        #it 'should call the model method that finds a movie in the database' do
        #    Movie.should_receive(:find).with(5)
        #    get: find_with_same_director, {:id =>5}
        #end
        it 'should call the model method that finds all movies with the same
            director' do
            fake_movie = mock('Movie')
            fake_results = [mock('Movie'), mock('Movie')]
            Movie.stub(:find).and_return(fake_movie)
            #fake_movie.stub(:director).and_return('Alfonso Cauron')
            #fake_results = [mock('Movie', :director => 'Alfonso Cauron'), 
                            #mock('Movie', :director => 'Alfonso Cauron')]
            #Movie.stub(:find).and_return(fake_movie)
            fake_movie.should_receive(:movies_with_same_director).and_return(
                fake_results)
            get :find_with_same_director, {:id => 5}
        end

        context 'when results are not empty' do 
            it 'should make the results available to the view' do
                fake_movie = mock('Movie')
                #fake_movie.stub(:director).and_return('Alfonso Cauron')
                fake_results = [mock('Movie'), mock('Movie')]
                Movie.stub(:find).and_return(fake_movie)
                fake_movie.stub(:movies_with_same_director).and_return(
                    fake_results)
                get :find_with_same_director, {:id => 5}
                #the controller method will assign results to @movies
                assigns(:movies).should == fake_results
            end
        
            it 'should select the search results template for rendering' do
                fake_movie = mock('Movie')
                #fake_movie.stub(:director)
                fake_results = [mock('Movie'), mock('Movie')]
                Movie.stub(:find).and_return(fake_movie)
                fake_movie.stub(:movies_with_same_director).and_return(
                    fake_results)
                get :find_with_same_director, {:id => 5}
                response.should render_template('find_with_same_director')
            end
        end

        context 'when the results are empty' do
            it 'should redirect to the index page' do
                fake_movie = mock('Movie', :title => 'Alfonso Cauron')
                #fake_movie.stub(:director)
                Movie.stub(:find).and_return(fake_movie)
                fake_movie.stub(:movies_with_same_director).and_return([])
                #ActionController::Base.should_receive(
                #    :redirect_to).with(movie_path(5))
                #response.should render_template('show')
                get :find_with_same_director, {:id => 5}
                expect(response).to redirect_to movies_path
            end
        end
    end
end
