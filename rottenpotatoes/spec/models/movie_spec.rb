require 'spec_helper'

describe Movie do
    describe 'searching the database for movies by director' do
        context 'with a valid director' do
            it 'should call the find method by director' do
                fake_movie = mock('Movie', :empty? => false)
                Movie.should_receive(:find).with(
                    :all, 
                    :conditions => {:director => 'Alfonso Cauron'}).and_return(fake_movie)
                @movie = Movie.new
                @movie.director = 'Alfonso Cauron'
                @movie.save!
                @movie.movies_with_same_director
            end

            context 'with a director in the database' do 
                it 'should return movies by the same director' do
                    fake_results = [mock('Movie', :title => 'Children of Men', 
                                     :director => 'Alfonso Cauron'), 
                                     mock('Movie', :title => 'Y Tu Mama Tambien', 
                                          :director => 'Alfonso Cauron')]
                    Movie.stub(:find).and_return(fake_results)
                    @movie = Movie.new
                    @movie.director = "Alfonso Cauron"
                    @movie.save!
                    @movie.movies_with_same_director.should == fake_results
                end
            end
            
            context 'with a director not in the database' do
                it 'should return an empty array' do
                    Movie.stub(:find).and_return([])
                    @movie = Movie.new
                    @movie.movies_with_same_director.should be_empty
                end
            end
        end

        context 'without valid director' do
            it 'should not call the find method' do
                Movie.should_not_receive(:find)
                @movie = Movie.new
                @movie.director = ''
                @movie.save!
                @movie.movies_with_same_director
            end

            it 'should return an empty array' do
                Movie.stub(:find).and_return([])
                @movie = Movie.new
                #@movie.director = ''
                #@movie.save!
                @movie.movies_with_same_director.should be_empty
            end
        end
    end
end
        
