Given /the following movies exist:/ do |movies_table|
    movies_table.hashes.each do |movie|
        Movie.create(movie)
    end
end

#Given /(?:|I) am on the details page for \"(.+)\"/ do |movie_title|
   #step "I go to the edit page for \"#{movie_title}\""     
#end

#When /(?:|I) go to the edit page for \"(.+)\"/ do |movie_title|
#    id = Movie.find(:first, :conditions => {:title => movie_title}).id
#    visit edit_movie_path(id)
#end

Then /the director of "(.+)" should be "(.+)"/ do |movie, director|
    steps %Q{Then I should see "#{movie}"
             And I should see "#{director}"
            }
end

#Then /(?:|I) should be on (.+) for \"(.+)\"/ do |page_name, movie_title|
#    step "I should be on 
#    step "I should see \"#{movie_title}\""
#end
