class Movie < ActiveRecord::Base

  attr_accessible :title, :rating, :description, :release_date, :director

  def self.all_ratings
    %w(G PG PG-13 NC-17 R)
  end

  def movies_with_same_director
    if director == '' or director.nil?
        return []
    end
    similar_movies = Movie.find(:all, :conditions => {:director => director})
    similar_movies.empty? ? [] : similar_movies
  end
end

